################################################################################
## Ansible Settings
################################################################################

ansible_python_interpreter: auto
swap_file_size_mb: "2048"
swap_swappiness: "10"
system_packages: "aptitude curl openssh-server ca-certificates glances nano tzdata ack-grep tree python3-pip nfs-common postfix jq"
internal_cidr_16: "{{ (ansible_default_ipv4.address + '/16') | ipaddr('network/prefix') }}"

################################################################################
## Cloud Provider Settings
################################################################################

internal_ip_lookup: {gcp: ['networkInterfaces', 0, 'networkIP'], azure: ['private_ipv4_addresses', 0], aws: ['private_ip_address'], alicloud: ['private_ip_address'], tencentcloud: ['private_ip_address']}
external_ip_lookup: {gcp: ['networkInterfaces', 0, 'accessConfigs', 0, 'natIP'], azure: ['public_ipv4_addresses', 0], aws: ['public_ip_address'], alicloud: ['public_ip_address'], tencentcloud: ['public_ip_address'] }

## GCP
gcp_service_account_host_file: "{{ service_account_file | default('', true) }}"
gcp_service_account_target_file: "/etc/gitlab/serviceaccount.json"
gcp_project: "{{ project_name | default('', true) }}"
gcp_zone: ""

## AWS
aws_region: ""

################################################################################
## GitLab Install Settings
################################################################################

gitlab_version: ""
gitlab_edition: "gitlab-ee"
## Set to env var, package with version wildcard or just latest
gitlab_repo_package: "{{ lookup('env','GITLAB_REPO_PACKAGE') | default(gitlab_edition + '=' + gitlab_version + '*' if gitlab_version != '' else 'gitlab-ee', true) }}"
gitlab_repo_script_url: "{{ lookup('env','GITLAB_REPO_SCRIPT_URL') | default('https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh', true) }}"
## Specify absolute path to the local deb package on host
gitlab_deb_host_path: "{{ lookup('env','GITLAB_HOST_DEB_PATH')}}"
gitlab_deb_target_path: "{{ lookup('env','GITLAB_TARGET_DEB_PATH') | default('/tmp/' + (gitlab_deb_host_path | basename), true)}}"

external_url_sanitised: "{{ external_url | regex_replace('\\/$', '') }}"
omnibus_node: true
gitlab_admin_email: "admin@example.com"

# Object Storage Buckets
## Will be switched to separate buckets by default in future release
gitlab_object_storage_artifacts_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_backups_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_dependency_proxy_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_external_diffs_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_lfs_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_packages_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_terraform_state_bucket: "{{ prefix }}-object-storage"
gitlab_object_storage_uploads_bucket: "{{ prefix }}-object-storage"

################################################################################
## GitLab Component Settings (Omnibus)
################################################################################

# Consul
consul_int_ips: "{{ (groups['consul'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'consul' in groups else [] }}"

# GitLab Rails (Application nodes)
gitlab_rails_int_ips: "{{ (groups['gitlab_rails'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'gitlab_rails_primary' in groups else [] }}"

# Gitaly
gitaly_primary_int_ip: "{{ (groups['gitaly_primary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'gitaly_primary' in groups else '' }}"
gitaly_secondary_int_ips: "{{ (groups['gitaly_secondary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'gitaly_secondary' in groups else [] }}"

# Praefect
praefect_primary_int_ip: "{{ (groups['praefect_primary'] | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'praefect_primary' in groups else '' }}"
praefect_secondary_int_ips: "{{ (groups['praefect_secondary'] | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'praefect_secondary' in groups else [] }}"
praefect_int_ips: "{{ (groups['praefect'] | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'praefect' in groups else [] }}"

praefect_postgres_int_ip: "{{ (groups['praefect_postgres'] | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'praefect_postgres_primary' in groups else '' }}"
praefect_postgres_dbname: "praefect_production"
praefect_postgres_username: "praefect"

# GitLab Monitor
monitor_int_ip: "{{ (groups['monitor'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'monitor' in groups else '' }}"

# GitLab Postgres / PGBouncer
postgres_primary_int_ip: "{{ (groups['postgres_primary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'postgres_primary' in groups else '' }}"
postgres_primary_int_port: "5432"
postgres_replication_manager: "{{ 'patroni' if ((groups['postgres'] is defined) and (groups['postgres'] | length > 1)) else 'none' }}"

pgbouncer_int_ips: "{{ (groups['pgbouncer'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'pgbouncer' in groups else [] }}"

# GitLab Redis
redis_port: 6379
redis_sentinel_port: 26379

redis_primary_int_ip: "{{ (groups['redis_primary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'redis' in groups else '' }}"
redis_sentinel_int_ips: "{{ (groups['redis_sentinel'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'redis_sentinel' in groups else [] }}"

redis_cache_primary_int_ip: "{{ (groups['redis_cache_primary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'redis_cache' in groups else '' }}"
redis_sentinel_cache_int_ips: "{{ (groups['redis_sentinel_cache'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'redis_sentinel_cache' in groups else [] }}"

redis_persistent_primary_int_ip: "{{ (groups['redis_persistent_primary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'redis_persistent' in groups else '' }}"
redis_sentinel_persistent_int_ips: "{{ (groups['redis_sentinel_persistent'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'redis_sentinel_persistent' in groups else [] }}"

# Sidekiq
sidekiq_max_concurrency: 10

################################################################################
## GitLab Component Settings (Non-Omnibus)
################################################################################

# HAProxy
haproxy_internal_int_ip: "{{ (groups['haproxy_internal'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'haproxy_internal' in groups else ''}}"
haproxy_external_int_ip: "{{ (groups['haproxy_external'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'haproxy_external' in groups else '' }}"
haproxy_external_ext_ip: "{{ (groups['haproxy_external'] | sort | map('extract', hostvars, external_ip_lookup[cloud_provider]) | join('')) if 'haproxy_external' in groups else '' }}"

# GitLab NFS
gitlab_nfs_int_ip: "{{ groups['gitlab_nfs'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('') }}"
gitlab_nfs_path: "/mnt/gitlab-nfs"

# Elastic
elasticsearch_clean_install: "{{ lookup('env','ELASTICSEARCH_CLEAN_INSTALL') | default('false', true) }}"
elastic_primary_int_ip: "{{ (groups['elastic_primary'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'elastic_primary' in groups else '' }}"
elastic_int_ips: "{{ (groups['elastic'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | list) if 'elastic' in groups else [] }}"
elasticsearch_urls: "{{ (elastic_int_ips | map('regex_replace', '^(.*)$', 'http://\\1:9200') | list) if elastic_int_ips | length > 0 else [] }}"
elasticsearch_replicas: "{{ 1 if (groups['elastic'] | length > 1 ) else 0 }}"
elasticsearch_shards: "{{ (((groups['elastic'] | sort | map('extract', hostvars, ['ansible_processor_vcpus']) | list | sum) / (elasticsearch_replicas | int + 1)) | round | int) if 'elastic' in groups else 0 }}"

# Jaeger Distributed Tracing
jaeger_int_ip: "{{ (groups['jaeger'] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) if 'jaeger' in groups else '' }}"

################################################################################
## GitLab Cloud Native Hybrid Settings (Helm)
################################################################################

kubeconfig_setup: false

gitlab_charts_release_namespace: default

## Webservice
gitlab_charts_webservice_requests_memory_gb: 5
gitlab_charts_webservice_limits_memory_gb: 5.25
gitlab_charts_webservice_requests_cpu: 4
gitlab_charts_webservice_min_replicas_scaler: 0.75
gitlab_charts_webservice_max_replicas: ""
gitlab_charts_webservice_min_replicas: ""

## Sidekiq
gitlab_charts_sidekiq_requests_memory_gb: 2
gitlab_charts_sidekiq_limits_memory_gb: 4
gitlab_charts_sidekiq_requests_cpu: 0.9
gitlab_charts_sidekiq_min_replicas_scaler: 0.75
gitlab_charts_sidekiq_max_replicas: ""
gitlab_charts_sidekiq_min_replicas: ""

################################################################################
## GitLab Post Configure Settings
################################################################################

access_token_user: "root"
access_token_key: "{{ lookup('password', 'tmp/access-token chars=ascii_lowercase,digits length=20') }}"
access_token_scopes: "[:api]"

################################################################################
## GitLab Geo Settings
################################################################################

geo_primary_site_postgres_group_name: "{% if 'geo_primary_site_postgres_primary' in groups %}geo_primary_site_postgres_primary{% elif 'geo_primary_site_gitlab_rails_primary' in groups %}geo_primary_site_gitlab_rails_primary{% elif 'postgres_primary' in groups%}postgres_primary{% else %}gitlab_rails_primary{% endif %}"
geo_primary_site_postgres_int_ip: "{{ (groups[geo_primary_site_postgres_group_name] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) }}"
geo_secondary_site_postgres_group_name: "{% if 'geo_secondary_site_postgres_primary' in groups %}geo_secondary_site_postgres_primary{% elif 'geo_secondary_site_gitlab_rails_primary' in groups %}geo_secondary_site_gitlab_rails_primary{% else %}gitlab_rails_primary{% endif %}"
geo_secondary_site_postgres_int_ip: "{{ (groups[geo_secondary_site_postgres_group_name] | sort | map('extract', hostvars, internal_ip_lookup[cloud_provider]) | join('')) }}"
