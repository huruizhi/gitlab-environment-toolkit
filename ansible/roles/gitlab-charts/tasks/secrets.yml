---
- name: Lookup GitLab Secrets file from NFS
  slurp:
    path: "{{ gitlab_nfs_path }}/gitlab-secrets.json"
  delegate_to: "{{ groups['gitlab_nfs'][0] }}"
  delegate_facts: true
  become: true
  register: gitlab_secrets_json
  tags:
    - secrets
    - rails-secrets

- name: Convert GitLab Rails secrets from file
  set_fact:
    gitlab_rails_secrets:
      production: "{{ (gitlab_secrets_json.content | b64decode | from_yaml).gitlab_rails }}"
    gitlab_shell_token: "{{ (gitlab_secrets_json.content | b64decode | from_yaml).gitlab_shell.secret_token }}"
  tags:
    - secrets
    - rails-secrets

- name: Configure GitLab Rails secrets
  k8s:
    state: present
    definition:
      kind: Secret
      type: Opaque
      metadata:
        name: "gitlab-rails-secrets"
        namespace: "{{ gitlab_charts_release_namespace }}"
      stringData:
        secrets.yml: |
          {{ gitlab_rails_secrets | to_yaml }}
  tags:
    - secrets
    - rails-secrets

- name: Configure GitLab Shell Token Secret
  k8s:
    state: present
    definition:
      kind: Secret
      type: Opaque
      metadata:
        name: "gitlab-shell-token"
        namespace: "{{ gitlab_charts_release_namespace }}"
      stringData:
        password: "{{ gitlab_shell_token }}"
  tags:
    - secrets
    - rails-secrets

- name: Configure GitLab Chart password secrets
  k8s:
    state: present
    definition:
      kind: Secret
      type: Opaque
      metadata:
        name: "{{ item.name }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
      stringData:
        password: "{{ item.secret }}"
  no_log: true
  loop:
    - { name: "gitlab-initial-root-password", secret: "{{ gitlab_root_password }}" }
    - { name: "gitlab-postgres-password", secret: "{{ postgres_password }}" }
    - { name: "gitlab-redis-password", secret: "{{ redis_password }}" }
    - { name: "gitlab-praefect-external-token", secret: "{{ praefect_external_token }}" }
    - { name: "gitlab-grafana-initial-password", secret: "{{ grafana_password }}" }
  tags: secrets

- name: Configure Object Storage connection key
  k8s:
    state: present
    definition:
      kind: Secret
      type: Opaque
      metadata:
        name: "gitlab-object-storage-key"
        namespace: "{{ gitlab_charts_release_namespace }}"
      stringData:
        key: "{{ lookup('template', 'templates/object_storage_key.gcp.yml.j2') }}"
  tags: secrets

- name: Configure Backups Object Storage connection key
  k8s:
    state: present
    definition:
      kind: Secret
      type: Opaque
      metadata:
        name: "gitlab-backups-object-storage-key"
        namespace: "{{ gitlab_charts_release_namespace }}"
      stringData:
        key: |
          {{ lookup('file', gcp_service_account_host_file) | tojson }}
  tags:
    - secrets
    - backup-secrets
